

import { useEffect, useState } from "react";
import "./App.css"

//https://Luiz_eh@bitbucket.org/programacao-4/curriculo-app.git


 const AppHook = () => {

    const [nome, setNome] = useState('')
    const [email, setEmail] = useState('')
    const [fone, setFone] = useState('')
    const [cargo, setCargo] = useState('')
    const [historicoPro, setHistoricoPro] = useState('')
    const [viewType, setViewType] = useState('')
    const [validation, setValidation] = useState({
        form: false,
        nome: null,
        email: null,
        fone: null,
        cargo: null,
        historicoPro: null,
    })

    const formRef = useRef()
    const nomeRef = useRef()
    const emailRef = useRef()
    const foneRef = useRef()
    const cargoRef = useRef()
    const historicoProRef = useRef()

    
    const handleInput = (event, fieldRef, update) => {
        let fieldName = event.target.name

        let currentValidation = validation
        currentValidation.form = formRef.current.checkValidity()
        currentValidation[fieldName] = fieldRef.current.validity

        update(event.target.value)
        setValidation(currentValidation)

    }

        return (
            <form ref={formRef}> 

              { this.state.viewType === 0 &&
              
              <div>
                
                <h4 id="nome">Informe o nome:</h4>
                  <input className="styleAll" type="text" name="Name" value={nome} ref={nomeRef}
                      required onChange={event => handleInput(event, nomeRef, setNome)} />

                  {
                      validation.nome &&
                      !validation.nome.valid &&
                      <div classNome="error-message">Nome é obrigatório</div>
                  }

                  <br /> <br />

                  <h4 id="nome">Informe o e-mail:</h4> 
                  <input className="styleAll" type="email" name="email" value={email} ref={emailRef}
                      required onChange={event => handleInput(event, emailRef, setEmail)} />

                  {
                      validation.email &&
                      validation.email.valueMissing &&
                      <div className="error-message">E-mail é obrigatório</div>
                  }

                  {
                      validation.email &&
                      validation.email.valueMissing &&
                      !validation.email.valid &&
                      <div className="error-message">E-mail inválido</div>
                  }

                  <br /> <br />

                  <h4 id="nome">Informe Número de telefone:</h4> 
                  <input className="styleAll" type="tel" name="telefone" value={fone} ref={foneRef}
                      required onChange={event => this.handleInput(event, foneRef, setFone)} />

                  {
                      validation.fone &&
                      !validation.fone.valid &&
                      <div className="error-message">Número de telefone é obrigatório</div>
                  }

                  <br /> <br />

                  <h4 id="nome">Informe o cargo:</h4> 
                  <input className="styleAll" type="text" name="cargo" value={cargo} ref={cargoRef}
                      required onChange={event => handleInput(event, cargoRef, setCargo)} />

                  {
                      validation.cargo && 
                      !validation.cargo.valid && 
                      <div className="error-message">Cargo obrigatório</div>
                  }

                  <br /> <br />

                  <h4 id="nome">Informe o histórico:</h4>
                  <textarea className="txtbox" type="text" name="historicoPro" value={historicoPro} ref={historicoProRef}
                      required onChange={event => handleInput(event, historicoProRef, setHistoricoPro)} />

                  {
                      validation.historicoPro && 
                      !validation.historicoPro.valid && 
                      <div className="error-message">Histórico obrigatório</div>
                  }

                  <br /> <br />

                  <input className="styleAll" type="button" value="Salvar"
                    disabled={!validation.form} 
                    onClick={() => setViewType(1)}/>

              </div>

              }

              {viewType === 1 && 
              
              <div>

                 <div className="styleName">{nome}</div>
                 <br />

                <h3 id="nome">Email:</h3>
                 <div className="styleAll">{email}</div>
                 <br />

                 <h3 id="nome">Telefone:</h3>
                 <div className="styleAll">{fone}</div>
                 <br />

                 <h3 id="nome">Cargo:</h3>
                 <div className="styleAll">{cargo}</div>
                 <br />

                 <h3 id="nome">Histórico proficional:</h3>
                 <div className="styleAll">{historicoPro}</div>
                 <br />

                 <input className="styleAll" type='button' value="Adicionar novo Curriculo"
                 onClick={() => setViewType(0) }/>

              </div>

              }


              {viewType === 2 &&
              
              <div>

                    <input className="bottonCadastro" type="button" value="Cadastrar" 
                    onClick={() => setViewType(0) }/>

              </div>
              
              }

            </form>
        )
    }




export default AppHook;
